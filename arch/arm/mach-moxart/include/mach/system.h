/* Copyright (C) 2013 Jonas Jensen <jonas.jensen@gmail.com>
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version. */

#ifndef __MACH_SYSTEM_H
#define __MACH_SYSTEM_H

#include <linux/io.h>
#include <mach/hardware.h>

static inline void arch_idle(void)
{
	/*
	 * Because of broken hardware we have to enable interrupts or the CPU
	 * will never wakeup... Acctualy it is not very good to enable
	 * interrupts here since scheduler can miss a tick, but there is
	 * no other way around this. Platforms that needs it for power saving
	 * should call enable_hlt() in init code, since by default it is
	 * disabled.
	 */
	local_irq_enable();
	cpu_do_idle();
}


/*
#define RESET_GLOBAL		(1 << 31)
#define RESET_CPU1			(1 << 30)
#define GLOBAL_RESET		0x0C
*/

static inline void arch_reset(char mode, const char *cmd)
{
	/* __raw_writel(RESET_GLOBAL | RESET_CPU1,
		IO_ADDRESS(MOXART_GLOBAL_BASE) + GLOBAL_RESET); */

	if (mode == 's') {	/* Jump to ROM address 0 */
		cpu_reset(0);
	} else {					/* reset the CPU */
		/* set counter to 1 */
		__raw_writel(1, IO_ADDRESS(MOXART_WATCHDOG_BASE)+4);

		/* start the watch dog */
		__raw_writel(0x5ab9, IO_ADDRESS(MOXART_WATCHDOG_BASE)+8);

		/* set to reset the CPU */
		__raw_writel(0x03, IO_ADDRESS(MOXART_WATCHDOG_BASE)+12);
	}
}

#endif /* __MACH_SYSTEM_H */
