/* Copyright (C) 2013 Jonas Jensen <jonas.jensen@gmail.com>
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version. */

#include <linux/init.h>
#include <linux/io.h>
#include <linux/ioport.h>
#include <linux/stddef.h>
#include <linux/list.h>
#include <linux/sched.h>
#include <asm/irq.h>
#include <asm/mach/irq.h>
#include <mach/hardware.h>

static void moxart_ack_irq(unsigned int irq)
{
	if (irq < 32)
		__raw_writel(1 << irq,
			IRQ_CLEAR(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	if (irq < 64) {
		irq -= 32;
		__raw_writel(1 << irq,
			FIQ_CLEAR(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	}
}

static void moxart_mask_irq(unsigned int irq)
{
	unsigned int mask;
	if (irq < 32) {
		mask = __raw_readl(IRQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
		mask &= ~(1 << irq);
		__raw_writel(mask, IRQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	}
	if (irq < 64) {
		irq -= 32;
		mask = __raw_readl(FIQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
		mask &= ~(1 << irq);
		__raw_writel(mask, FIQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	}
}

static void moxart_unmask_irq(unsigned int irq)
{
	unsigned int mask;
	if (irq < 32) {
		mask = __raw_readl(IRQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
		mask |= (1 << irq);
		__raw_writel(mask, IRQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	}
	if (irq < 64) {
		irq -= 32;
		mask = __raw_readl(FIQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
		mask |= (1 << irq);
		__raw_writel(mask, FIQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	}
}

void moxart_generic_set_generic(unsigned int base, unsigned int irq,
	unsigned int low)
{
	unsigned int mask;
	mask = __raw_readl(base);
	(low) ? (mask |= (1 << irq)) : (mask &= ~(1 << irq));
	__raw_writel(mask, base);
}

/*void __init moxart_int_set_irq(unsigned int irq, int mode, int level)*/
void moxart_int_set_irq(unsigned int irq, int mode, int level)
{
	if (irq < 32) {
		moxart_generic_set_generic(
			IRQ_TMODE(IO_ADDRESS(MOXART_INTERRUPT_BASE)),
			irq, mode);
		moxart_generic_set_generic(
			IRQ_TLEVEL(IO_ADDRESS(MOXART_INTERRUPT_BASE)),
			irq, level);
	}
	if (irq < 64) {
		irq -= 32;
		moxart_generic_set_generic(
			FIQ_TMODE(IO_ADDRESS(MOXART_INTERRUPT_BASE)),
			irq, mode);
		moxart_generic_set_generic(
			FIQ_TLEVEL(IO_ADDRESS(MOXART_INTERRUPT_BASE)),
			irq, level);
	}
}

static struct irq_chip moxart_irq_chip = {
	.name	= "INTC",
	.ack	= moxart_ack_irq,
	.mask	= moxart_mask_irq,
	.unmask	= moxart_unmask_irq,
};

static struct resource irq_resource = {
	.name	= "irq_handler",
	.start	= IO_ADDRESS(MOXART_INTERRUPT_BASE),
	.end	= IO_ADDRESS(FIQ_STATUS(MOXART_INTERRUPT_BASE)) + 4,
};

void __init moxart_init_irq(void)
{
	unsigned int mode = 0, level = 0, irq;

	disable_hlt();

	request_resource(&iomem_resource, &irq_resource);

	for (irq = 0; irq < NR_IRQS; irq++) {
		set_irq_chip(irq, &moxart_irq_chip);
		if (irq == IRQ_TIMER1 || irq == IRQ_TIMER2 ||
			irq == IRQ_TIMER3) {
			set_irq_handler(irq, handle_edge_irq);
			mode |= (1 << irq);
			level |= (1 << irq);
		} else {
			set_irq_handler(irq, handle_level_irq);
		}
		set_irq_flags(irq, IRQF_VALID | IRQF_PROBE);
	}
	/* pr_info("IRQ chip/handler/flags assign finished\r\n"); */

	__raw_writel(0, IRQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	__raw_writel(0, FIQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));

	__raw_writel(0xffffffff, IRQ_CLEAR(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	__raw_writel(0xffffffff, FIQ_CLEAR(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	/* pr_info("IRQ init finished\n"); */

	__raw_writel(mode, IRQ_TMODE(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	__raw_writel(level, IRQ_TLEVEL(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	__raw_writel(0, FIQ_TMODE(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	__raw_writel(0, FIQ_TLEVEL(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	/* pr_info("IRQ mode/level setup finished\n"); */

	pr_info("MOXART CPU IRQ: init_irq finished\n");
}
