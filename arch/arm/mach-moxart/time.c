/* Copyright (C) 2013 Jonas Jensen <jonas.jensen@gmail.com>
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version. */

#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/io.h>
#include <linux/ioport.h>

#include <mach/hardware.h>
#include <asm/mach/time.h>

#define TIMER_1_COUNT(base_addr)		(base_addr + 0x00)
#define TIMER_1_LOAD(base_addr)			(base_addr + 0x04)
#define TIMER_1_MATCH1(base_addr)		(base_addr + 0x08)
#define TIMER_1_MATCH2(base_addr)		(base_addr + 0x0C)

#define TIMER_2_COUNT(base_addr)		(base_addr + 0x10)
#define TIMER_2_LOAD(base_addr)			(base_addr + 0x14)
#define TIMER_2_MATCH1(base_addr)		(base_addr + 0x18)
#define TIMER_2_MATCH2(base_addr)		(base_addr + 0x1C)

#define TIMER_3_COUNT(base_addr)		(base_addr + 0x20)
#define TIMER_3_LOAD(base_addr)			(base_addr + 0x24)
#define TIMER_3_MATCH1(base_addr)		(base_addr + 0x28)
#define TIMER_3_MATCH2(base_addr)		(base_addr + 0x2C)

#define TIMER_CR(base_addr)				(base_addr + 0x30)

#define TIMER_1_CR_ENABLE(base_addr)	(base_addr + 0x30)
#define TIMER_1_CR_EXTCLK(base_addr)	(base_addr + 0x34)
#define TIMER_1_CR_FLOWIN(base_addr)	(base_addr + 0x38)

#define TIMER_2_CR_ENABLE(base_addr)	(base_addr + 0x42)
#define TIMER_2_CR_EXTCLK(base_addr)	(base_addr + 0x46)
#define TIMER_2_CR_FLOWIN(base_addr)	(base_addr + 0x50)

#define TIMER_3_CR_ENABLE(base_addr)	(base_addr + 0x54)
#define TIMER_3_CR_EXTCLK(base_addr)	(base_addr + 0x58)
#define TIMER_3_CR_FLOWIN(base_addr)	(base_addr + 0x62)

#define TIMER_INTR_STATE(base_addr)		(base_addr + 0x34)

#define TIMEREG_1_CR_ENABLE			(1 << 0)
#define TIMEREG_1_CR_CLOCK			(1 << 1)
#define TIMEREG_1_CR_INT			(1 << 2)
#define TIMEREG_2_CR_ENABLE			(1 << 3)
#define TIMEREG_2_CR_CLOCK			(1 << 4)
#define TIMEREG_2_CR_INT			(1 << 5)
#define TIMEREG_3_CR_ENABLE			(1 << 6)
#define TIMEREG_3_CR_CLOCK			(1 << 7)
#define TIMEREG_3_CR_INT			(1 << 8)
#define TIMEREG_COUNT_UP			(1 << 9)
#define TIMEREG_COUNT_DOWN			(0 << 9)

/* is it needed? (defined to CONFIG_HZ in other drivers)
 * arch/arm/Kconfig's config HZ is default 100. */
/* #define HZ			1000 */

#define MAX_TIMER	2
#define USED_TIMER	1

#define TIMER1_COUNT                0x0
#define TIMER1_LOAD                 0x4
#define TIMER1_MATCH1               0x8
#define TIMER1_MATCH2               0xC
#define TIMER2_COUNT                0x10
#define TIMER2_LOAD                 0x14
#define TIMER2_MATCH1               0x18
#define TIMER2_MATCH2               0x1C
#define TIMER3_COUNT                0x20
#define TIMER3_LOAD                 0x24
#define TIMER3_MATCH1               0x28
#define TIMER3_MATCH2               0x2C
#define TIMER_INTR_MASK		0x38

static irqreturn_t moxart_timer_interrupt(int irq, void *dev_id)
{
	/* printk("TIMER moxart_timer_interrupt irq = %d\n", irq); */
	/* __raw_writel(0, TIMER_INTR_STATE(IO_ADDRESS(MOXART_TIMER1_BASE))); */

	timer_tick();

	return IRQ_HANDLED;
}

static struct irqaction moxart_timer_irq = {
	.name		= "MOXART Timer Tick",
	.flags		= IRQF_DISABLED | IRQF_TIMER,
	.handler	= moxart_timer_interrupt,
};

/*static struct resource timer_resource = {
	.name	= "timer_handler",
	.start	= IO_ADDRESS(MOXART_TIMER_BASE),
	.end	= IO_ADDRESS(TIMER_INTR_STATE(MOXART_TIMER_BASE)) + 4,
};*/

void __init moxart_timer_init(void)
{
	/* unsigned int t_int; */

	setup_irq(IRQ_TIMER1, &moxart_timer_irq);

	/*t_int = __raw_readl(TIMER_INTR_STATE(IO_ADDRESS(MOXART_TIMER1_BASE)));
	printk("TIMER TIMER_INTR_STATE(IO_ADDRESS(MOXART_TIMER1_BASE))"
		" = %d\n", t_int);
	t_int = __raw_readl(TIMER_1_COUNT(IO_ADDRESS(MOXART_TIMER1_BASE)));
	printk("TIMER TIMER_1_COUNT(IO_ADDRESS(MOXART_TIMER1_BASE))"
		" = %d\n", t_int);*/

	__raw_writel(APB_CLK / HZ,
		TIMER_1_COUNT(IO_ADDRESS(MOXART_TIMER_BASE)));
	__raw_writel(APB_CLK / HZ,
		TIMER_1_LOAD(IO_ADDRESS(MOXART_TIMER_BASE)));

	/*
	__raw_writel(0xffffffff, TIMER_1_MATCH1(IO_ADDRESS(MOXART_TIMER_BASE)));
	__raw_writel(0xffffffff, TIMER_1_MATCH2(IO_ADDRESS(MOXART_TIMER_BASE)));
	*/

	/*
	__raw_writel(TIMER_1_CR_ENABLE | TIMER_1_CR_INT,
		TIMER_CR(IO_ADDRESS(MOXART_TIMER1_BASE)));
	__raw_writel((1 << 0) | (1 << 2),
		TIMER_1_CR_ENABLE(IO_ADDRESS(MOXART_TIMER_BASE)));
	__raw_writel(0x0000020f,
		TIMER_1_CR_ENABLE(IO_ADDRESS(MOXART_TIMER_BASE)));
	__raw_writel(TIMEREG_COUNT_UP | TIMEREG_1_CR_ENABLE |
		TIMEREG_1_CR_INT | TIMEREG_1_CR_CLOCK,
		TIMER_1_CR_ENABLE(IO_ADDRESS(MOXART_TIMER_BASE)));
	__raw_writel(TIMEREG_COUNT_UP | TIMEREG_1_CR_ENABLE |
		TIMEREG_1_CR_INT,
		TIMER_1_CR_ENABLE(IO_ADDRESS(MOXART_TIMER_BASE)));
	 */

	__raw_writel(1, TIMER_1_CR_ENABLE(IO_ADDRESS(MOXART_TIMER_BASE)));
	__raw_writel(0, TIMER_1_CR_EXTCLK(IO_ADDRESS(MOXART_TIMER_BASE)));
	__raw_writel(1, TIMER_1_CR_FLOWIN(IO_ADDRESS(MOXART_TIMER_BASE)));

	/*t_int = __raw_readl(TIMER_INTR_STATE(IO_ADDRESS(MOXART_TIMER1_BASE)));
	printk("TIMER TIMER_INTR_STATE(IO_ADDRESS(MOXART_TIMER1_BASE))"
		" = %d\n", t_int);

	t_int = __raw_readl(TIMER_1_COUNT(IO_ADDRESS(MOXART_TIMER1_BASE)));
	printk("TIMER TIMER_1_COUNT(IO_ADDRESS(MOXART_TIMER1_BASE))"
		" = %d\n", t_int);
	t_int = __raw_readl(TIMER_1_LOAD(IO_ADDRESS(MOXART_TIMER1_BASE)));
	printk("TIMER TIMER_1_LOAD(IO_ADDRESS(MOXART_TIMER1_BASE))"
		" = %d\n", t_int);
	*/

	pr_info(
		"MOXART TIMER: count/load set (APB_CLK=%d / HZ=%d) IRQ=0x%x\n"
		, APB_CLK, HZ, IRQ_TIMER1);
}

struct sys_timer moxart_timer = {
	.init       = moxart_timer_init,
};

