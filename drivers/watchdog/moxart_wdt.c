/* MOXART Watchdog driver (based on MOXA sources)
 * Copyright (C) 2013 Jonas Jensen <jonas.jensen@gmail.com>
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version. */

#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/string.h>
#include <linux/watchdog.h>
#include <linux/reboot.h>
#include <linux/miscdevice.h>
#include <linux/poll.h>
#include <linux/timer.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/uaccess.h>
#include <linux/io.h>

#include <mach/hardware.h>

#define MOXA_WATCHDOG_MINOR			WATCHDOG_MINOR

/* 30 seconds */
#define DEFAULT_WATCHDOG_TIME		(30UL*1000UL)

/* 50 msec */
#define WATCHDOG_MIN_TIME			50UL

/* 60 seconds */
#define WATCHDOG_MAX_TIME			(60UL*1000UL)

/* 500 msec, for watchdog timer polling */
#define WATCHDOG_TOL_TIME			(500UL)

/* 1 seconds, for watchdog timer count */
#define WATCHDOG_TOL_COUNT_TIME		(1000UL)

#define WATCHDOG_COUNTER(x)	((APB_CLK/1000UL)*(x))
#define WATCHDOG_JIFFIES(x)	((((x)+WATCHDOG_TOL_TIME)*HZ)/1000UL)

/* enable watch dog and set time (unint msec) */
#define IOCTL_WATCHDOG_ENABLE		1

/* disable watch dog, kernle do it */
#define IOCTL_WATCHDOG_DISABLE		2

/* get now setting about mode and time */
#define IOCTL_WATCHDOG_GET_SETTING	3

/* to ack watch dog */
#define IOCTL_WATCHDOG_ACK			4

struct wdt_set_struct {
	int mode;
	unsigned long time;
};

static int opencounts;
static int wdt_user_enabled;
static unsigned long wdt_time = DEFAULT_WATCHDOG_TIME;
static struct timer_list wdt_timer;
static struct work_struct rebootqueue;

static void wdt_enable(void)
{
	__raw_writel(WATCHDOG_COUNTER(wdt_time + WATCHDOG_TOL_COUNT_TIME),
		IO_ADDRESS(MOXART_WATCHDOG_BASE) + 4);
	__raw_writel(0x5ab9, IO_ADDRESS(MOXART_WATCHDOG_BASE) + 8);
	__raw_writel(0x03, IO_ADDRESS(MOXART_WATCHDOG_BASE) + 12);
}

static void wdt_disable(void)
{
	__raw_writel(0, IO_ADDRESS(MOXART_WATCHDOG_BASE) + 12);
}

static int wdt_ioctl(struct inode *inode, struct file *file,
	unsigned int cmd, unsigned long arg)
{
	unsigned long time;
	struct wdt_set_struct nowset;
	unsigned long flags;

	switch (cmd) {
	case IOCTL_WATCHDOG_ENABLE:
		if (copy_from_user(&time, (void *) arg, sizeof(time)))
			return -EFAULT;
		if (time < WATCHDOG_MIN_TIME ||	time > WATCHDOG_MAX_TIME)
			return -EINVAL;
		local_irq_save(flags);
		if (wdt_user_enabled) {
			wdt_disable();
			del_timer(&wdt_timer);
		}
		wdt_time = time;
		wdt_user_enabled = 1;
		wdt_timer.expires = jiffies + WATCHDOG_JIFFIES(wdt_time);
		add_timer(&wdt_timer);
		wdt_enable();
		local_irq_restore(flags);
		break;
	case IOCTL_WATCHDOG_DISABLE:
		local_irq_save(flags);
		if (wdt_user_enabled) {
			wdt_disable();
			del_timer(&wdt_timer);
			wdt_user_enabled = 0;
		}
		local_irq_restore(flags);
		break;
	case IOCTL_WATCHDOG_GET_SETTING:
		nowset.mode = wdt_user_enabled;
		nowset.time = wdt_time;
		if (copy_to_user((void *) arg, &nowset,	sizeof(nowset)))
			return -EFAULT;
		break;
	case IOCTL_WATCHDOG_ACK:
		local_irq_save(flags);
		if (wdt_user_enabled) {
			wdt_disable();
			del_timer(&wdt_timer);
			wdt_timer.expires = jiffies +
				WATCHDOG_JIFFIES(wdt_time);
			add_timer(&wdt_timer);
			wdt_enable();
		}
		local_irq_restore(flags);
		break;
	default:
		return -EINVAL;
	}
	return 0;
}

static int wdt_open(struct inode *inode, struct file *file)
{
	unsigned long flags;

	dbg_printk(KERN_INFO "MOXART watchdog: wdt_open\n");
	if (MINOR(inode->i_rdev) != MOXA_WATCHDOG_MINOR)
		return -ENODEV;
	local_irq_save(flags);
	opencounts++;
	local_irq_restore(flags);
	return 0;
}

static int wdt_release(struct inode *inode, struct file *file)
{
	unsigned long flags;

	local_irq_save(flags);
	dbg_printk(KERN_INFO "MOXART watchdog: wdt_release\n");
	opencounts--;
	if (opencounts <= 0) {
		if (wdt_user_enabled) {
			wdt_disable();
			del_timer(&wdt_timer);
			wdt_user_enabled = 0;
		}
		opencounts = 0;
	}
	local_irq_restore(flags);
	return 0;
}

static void wdt_reboot(struct work_struct *work)
{
	char *argv[2], *envp[5];

	dbg_printk(KERN_INFO "MOXART watchdog: wdt_reboot\n");

/* With this define set, watchdog reset can be verified.
 * At boot: 10 second wdt_timer calls wdt_poll and after another
 * 10 seconds a reset is forced. However, in this driver, a user
 * mode reboot is preferred (but hardware watchdog should still work)
 * (unless lockup happens after wdt_disable()) */
/* #define DEBUG_TEST_WATCHDOG */
#ifdef DEBUG_TEST_WATCHDOG
	/*
	__raw_writel(APB_CLK*10, IO_ADDRESS(MOXART_WATCHDOG_BASE) + 4);
	__raw_writel(0x5ab9, IO_ADDRESS(MOXART_WATCHDOG_BASE) + 8);
	__raw_writel(0x03, IO_ADDRESS(MOXART_WATCHDOG_BASE) + 12);
	*/
#endif

	if (in_interrupt())
		return;
	/* if (!current->fs->root) return; */
	argv[0] = "/bin/reboot";
	argv[1] = 0;
	envp[0] = "HOME=/";
	envp[1] = "PATH=/sbin:/bin:/usr/sbin:/usr/bin";
	envp[2] = 0;
	call_usermodehelper(argv[0], argv, envp, 0);
}

static void wdt_poll(unsigned long ignore)
{
	unsigned long flags;

	local_irq_save(flags);
	wdt_disable();
#ifdef DEBUG_TEST_WATCHDOG
	wdt_enable();
#endif
	del_timer(&wdt_timer);
	pr_info("MOXART watchdog: Now reboot the system.\n");
	schedule_work(&rebootqueue);
	local_irq_restore(flags);
}

static int wdt_proc_output(char *buf)
{
	char *p;

	p = buf;
	p += sprintf(p, "user enable\t: %d\nack time\t: %d msec\n",
		wdt_user_enabled, (int) wdt_time);
	return p - buf;
}

static int wdt_read_proc(char *page, char **start, off_t off,
	int count, int *eof, void *data)
{
	int len = wdt_proc_output(page);

	if (len <= off + count)
		*eof = 1;
	*start = page + off;
	len -= off;
	if (len > count)
		len = count;
	if (len < 0)
		len = 0;
	return len;
}

static const struct file_operations moxart_wdt_fops = {
	.owner		= THIS_MODULE,
	.llseek		= no_llseek,
	.ioctl		= wdt_ioctl,
	.open		= wdt_open,
	.release	= wdt_release,
};

static struct miscdevice wdt_dev = {
	MOXA_WATCHDOG_MINOR,
	"moxart_watchdog",
	&moxart_wdt_fops
};

static void __exit moxart_wdt_exit(void)
{
	unsigned long	flags;

	local_irq_save(flags);
	if (wdt_user_enabled) {
		wdt_disable();
		del_timer(&wdt_timer);
		wdt_user_enabled = 0;
		opencounts = 0;
	}
	local_irq_restore(flags);
	misc_deregister(&wdt_dev);
}

static int __init moxart_wdt_init(void)
{
	if (misc_register(&wdt_dev)) {
		pr_info("MOXART watchdog: misc_register failed!\n");
		goto moxart_wdt_init_err;
	}

	opencounts = 0;
	wdt_user_enabled = 0;

	INIT_WORK(&rebootqueue, wdt_reboot);
	init_timer(&wdt_timer);
	wdt_timer.function = wdt_poll;

#ifdef DEBUG_TEST_WATCHDOG
	wdt_time = 10*1000;
	wdt_user_enabled = 1;
	wdt_timer.expires = jiffies + WATCHDOG_JIFFIES(wdt_time);
	add_timer(&wdt_timer);
	wdt_enable();
#endif

	create_proc_read_entry("driver/moxart_wdt", 0, 0, wdt_read_proc, NULL);

	pr_info("MOXART watchdog driver\n");

	return 0;

moxart_wdt_init_err:
	misc_deregister(&wdt_dev);
	return -ENOMEM;
}

module_init(moxart_wdt_init);
module_exit(moxart_wdt_exit);

MODULE_DESCRIPTION("MOXART watchdog driver");
MODULE_LICENSE("GPL");
